package Klijent.grafika;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class ProzorKlijent extends JFrame {
    private Container c;
    private BufferedReader in;
    private PrintWriter out;

    public ProzorKlijent() {
        setTitle("Vezba 9, Klijent");
        setSize(200, 200);
        uspostaviKomunikaciju();
        c = getContentPane();
        c.setLayout(new GridLayout(5, 1));
        JLabel l = new JLabel("KLIJENT ZA CHAT");
        c.add(l);
        JPanel p = new JPanel();
        p.setLayout(new GridLayout(1, 2));
        JLabel l1 = new JLabel("Ime:");
        p.add(l1);
        JTextField tf = new JTextField();
        p.add(tf);
        c.add(p);
        JLabel l2 = new JLabel("Poruka:");
        c.add(l2);
        JTextArea ta = new JTextArea();
        c.add(ta);
        JButton b = new JButton("Posalji");
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tf.setEnabled(false);
                out.println(tf.getText() + ":" + ta.getText());
                try {
                    in.readLine();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                JOptionPane.showMessageDialog(null, "Poruka uspesno poslata!");
                ta.setText("");
            }
        });
        c.add(b);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                out.println(tf.getText() + ":exit");
                System.exit(1);
            }
        });
    }

    private void uspostaviKomunikaciju() {

        try {
            Socket s = new Socket(InetAddress.getByName("127.0.0.1"), 9000);
            try {
                in = new BufferedReader(
                        new InputStreamReader(
                                s.getInputStream()));
                out = new PrintWriter(
                        new BufferedWriter(
                                new OutputStreamWriter(
                                        s.getOutputStream())), true);
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(c, "Greska pri konekciji sa serverom!");
                System.exit(1);
            }

        } catch (UnknownHostException e) {
            JOptionPane.showMessageDialog(c, "Greska pri konekciji sa serverom!");
            System.exit(1);
        } catch (IOException e) {
            JOptionPane.showMessageDialog(c, "Greska pri konekciji sa serverom!");
            System.exit(1);
        }
    }
}