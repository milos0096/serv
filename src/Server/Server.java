package Server;

import javax.swing.*;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server extends Thread {
    private JTextArea taPoruke;
    private JTextArea taKlijenti;

    public Server(JTextArea taPoruke, JTextArea taKlijenti) {
        this.taPoruke = taPoruke;
        this.taKlijenti = taKlijenti;
    }

    @Override
    public void run() {
        System.out.println("Server pokrenut!!");
        try {
            ServerSocket ss = new ServerSocket(9000);
            while (true) {
                Socket s = ss.accept();
                new ServerNit(s, taPoruke, taKlijenti).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
