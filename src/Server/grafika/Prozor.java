package Server.grafika;

import Server.Server;

import javax.swing.*;
import java.awt.*;

public class Prozor extends JFrame {
    private Container c;

    public Prozor() {
        setTitle("Vezba 9, Server");
        setSize(500, 500);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        c = getContentPane();
        c.setLayout(new BorderLayout());
        JLabel l = new JLabel("SERVER ZA CHAT");
        c.add("North", l);
        JTextArea taPoruke = new JTextArea();
        c.add("Center", taPoruke);
        JTextArea taKlijenti = new JTextArea();
        taKlijenti.append("Ucesnici:   \n");
        taKlijenti.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        c.add("East", taKlijenti);
        new Server(taPoruke, taKlijenti).start();
    }

}
