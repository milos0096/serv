package Server;

import javax.swing.*;
import java.io.*;
import java.net.Socket;

public class ServerNit extends Thread {
    private Socket s;
    private JTextArea taPoruke;
    private JTextArea taKlijenti;
    private BufferedReader in;
    private PrintWriter out;

    public ServerNit(Socket s, JTextArea taPoruke, JTextArea taKlijenti) {
        this.s = s;
        this.taPoruke = taPoruke;
        this.taKlijenti = taKlijenti;
        try {
            in = new BufferedReader(
                    new InputStreamReader(
                            s.getInputStream()));
            out = new PrintWriter(
                    new BufferedWriter(
                            new OutputStreamWriter(
                                    s.getOutputStream())), true);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void run() {
        System.out.println("Nov klijent");
        String p = null;
        try {
            p = in.readLine();
            taKlijenti.append(p.split(":")[0] + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
        taPoruke.append(p + "\n");
        out.println("potvrda");
        while (true) {
            try {
                p = in.readLine();
                if (p.split(":").length>1 && p.split(":")[1].equals("exit")) {
                    break;
                } else {
                    taPoruke.append(p + "\n");
                    out.println("potvrda");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        taPoruke.append(p.split(":")[0] + " napustio chat!!\n");
        String klijenti = taKlijenti.getText();
        taKlijenti.setText("");
        for (String klijent : klijenti.split("\n")) {
            if (klijent.equals(p.split(":")[0]))
                continue;
            taKlijenti.append(klijent + "\n");
        }
        try {
            in.close();
            out.close();
            s.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
